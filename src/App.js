import 'bootstrap/dist/css/bootstrap.min.css';
import avatar from './assets/images/avatar.jpg';

function App() {
  return (
    <div>
      <div style={{ width: "900px", margin: "100px auto", padding: "30px", textAlign: "center", border: "1px solid #ddd", backgroundColor: "bisque" }}>
        <div>
          <img src={avatar} alt="avatar User" style={{ borderRadius: "50%", width: "100px", marginTop: "-90px" }}></img>
        </div>
        <div style={{ fontSize: "20px" }}>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div>
          <span style={{color: "blue"}}>
            <b>
              Tammy Stevens
            </b>
          </span>
          <span style={{color: "brown"}}>&nbsp; * &nbsp;Front End Developer</span>
        </div>
      </div>
    </div>
  );
}

export default App;
